# ESLint Formatter for Bitbucket Code Insights

## References

- [ESLint documentation](https://eslint.org/docs/developer-guide/working-with-custom-formatters)
- [Bitbucket REST API documenation](https://developer.atlassian.com/cloud/bitbucket/rest/api-group-reports/#api-group-reports)
- [Bitbucket Code Insights documentation](https://support.atlassian.com/bitbucket-cloud/docs/code-insights/)
- [eslint-formatter-bitbucket-reports (npm)](https://www.npmjs.com/package/eslint-formatter-bitbucket-reports)
    - Decided not to use this because it's not very well-known and there are no tests.
    - [GitHub repository](https://github.com/spartez/eslint-formatter-bitbucket-reports)
- [ESLint & Bitbucket Server tutorial](https://developer.atlassian.com/server/bitbucket/tutorials-and-examples/code-insights-tutorial/)
    - Example code for Bitbucket Server, but still helpful
